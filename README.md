
# lcp - Linux Changelog Project

lcp is a program to parse and normalize changelog data from different 
linux distributions. This allows apples-to-apples comparions of 
package updates.

lcp is written in the D Programming Language but links to libexpat
for xml parsing.

https://dlang.org

## Supported Distributions

lcp currently supports parsing of changelog data for the following distros
- slackware
  - version 12 onwards
  - x86, x86_64
- fedora
  - version 7 onwards
  - x86_64
- rocky linux
  - all versions

## Requirements
- meson
- gcc-gdc
- expat
- slacklib (Slackware specific library.. can be installed on other distributions: https://gitlab.com/slvnt/slacklib/-/releases)

libgphobos if you want to reduce the file size by 
dynamically linking the D runtime library.


## How to build

### easy way
just run the build.sh script

```./build.sh```

This will build everything in the project directory. You can run lcp-download
and lcp from here to experiment with the code/data.

### manually with meson
If you want to build a package for global installation you can call the
meson build system directly

- meson build
- cd build
- meson compile

You can pass the -Dlocal_slacklib option to meson to build using a local copy of slacklib.
See build.sh for details.

## Using lcp

All data will be downloaded to your current working directory. 

### downloading upstream data
lcp-download will retrieve source data from all supported distros

```./lcp-download -d fedora -r 36 -a x86_64```

### parse the data
the "lcp" command parses the data that you just downloaded

```./lcp -d fedora -r 36 -a x86_64```

see 'lcp -h' for details
```
$ ./lcp -h
Usage: ./lcp <slackware|fedora|debian> <release name|number>

-d        Linux distro to parse
-r        dist_version
-a        dist_arch
-h --help This help information.
```
