import expat;
import std.stdio;
import std.string: toStringz, fromStringz, format, split, splitLines;
import std.conv : to;
import core.stdc.string : strlen;

import std.algorithm;
import std.file;

import std.typecons;
import std.net.curl;

alias FedoraMirrors = Tuple!(char[],char[]);


// Every Fedora release has 2 urls
// OS - Repo frozen at time of release
// UPDATES - all changes during post-release maintenance cycle
auto OS_URL = "https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-%s&arch=x86_64";
auto UPDATES_URL = "https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f%s&arch=x86_64";

// I'm bad at pointers... so this is a thing.
struct RepoData {
	string url;	
}


// uses the "API" to find a suitable mirror
// necessary because fedora moves their shit around in weird ways.
FedoraMirrors findMirror(string release) {
	auto content = get(OS_URL.format(release)).splitLines;
	auto os = content[content.length - 1];
	content = get(UPDATES_URL.format(release)).splitLines;
	auto updates = content[content.length - 1];
	return tuple(os, updates);
}


/* element handlers used to parse Fedora's repodata files */
extern (C) void elementStart_Repodata(void *userData, const XML_Char *name, const XML_Char **atts) {
	int i;
	RepoData *r = cast(RepoData *)userData;
	cast(void)atts;
		
	// get the name of the element we are working on
	auto elementName = fromStringz(name);
	
	if ( *atts == null) {
		//warn("%s %s".format(elementName, "has no attributes!!"));
		//return;
	}
	switch (elementName) {
		case "repomd": {
			//writeln(elementName);
			break;
		}
		case "location": {
			for (i = 0; i < strlen(*atts).to!int; i += 2) {
				switch (fromStringz(atts[i])) {
					case "href":  { 
						string u = fromStringz(atts[i+1]).to!string;
						if (u.endsWith("other.xml.gz") != 0 || u.endsWith("OTHER.xml.gz") != 0) {
							r.url = u; 
						}
						break;
					}
					default: {break;}
				}
			}
			break;
		}
		default: {break;}
	}
}

extern (C) void elementEnd_Repodata(void *data, const XML_Char *name){
}




string parseMD (string url) {
	RepoData data;
	/* Begin parsing with Expat */
	XML_Parser parser;
	// hold status returned by expat
	XML_Status status;
	
	char[] xmlText;
	int len;

	parser = XML_ParserCreate(null);
	//XML_UseParserAsHandlerArg(parser);
	XML_SetUserData(parser, &data);
	XML_SetStartElementHandler(parser, &elementStart_Repodata);
	XML_SetEndElementHandler(parser, &elementEnd_Repodata);
	
	xmlText = get(url);
	len = xmlText.length.to!int;
	
	status = XML_Parse(parser, xmlText.ptr, len, 1);
	
	if ( status == 0) {
		writeln("Cannot parse, file may be too large or not well-formed XML\n");
	}
	XML_ParserFree(parser);
	return data.url;
}



int main(string[] argv) {
	
	if (argv.length != 2) {
		return 1;
	}
	auto repomd = "repodata/repomd.xml";
	auto url = argv[1];
	
	auto u = parseMD(url ~ repomd);
	writeln(url ~ u);
	
	// sort the changelogs by date
	//sort!("a.date < b.date")(data.changelog);
	return 0;
}

