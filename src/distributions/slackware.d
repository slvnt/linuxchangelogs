import std.file      : exists;
import std.string    : format, split, splitLines, stripRight, strip;
import std.stdio     : writeln;
import std.algorithm : startsWith, findSplit, findSplitAfter, reverse, sort, count;
import std.ascii     : isWhite;
import std.array;
import std.regex;

import std.conv : to;
import std.process : execute;

import common;
import parser;

// use libslackware project to handle basic parsing tasks
import slacklib;

static string SEP = "+--------------------------+";

// not sure what I was gonna do with this
static string[] SERIES_LIST = ["a","ap","d","e","l","n","tcl","t","f","x","xap","xfce","kde","kdei","y","testing","exta","pasture",];

string[] SLACKWARE64 = ["13.0", "13.1", "13.37", "14.0", "14.1", "14.2", "15.0", "current" ];

int convertDateStamp(string datetime) {
	int dt = 0;
	
	// calling out to date command to do the conversion
	// this is stupidly slow
	auto proc = execute(["date", "-d %s".format(datetime), "+%s"]);
	dt = proc.output.stripRight.to!int;
	
	return dt;
}


class SlackwareParser : DistroParser {
	string path;
	string libdir_suffix = "";
	
	this(PARSER_CONTEXT newctx) {
		ctx = newctx;
		if (ctx.dist_arch == DIST_ARCH.x86_64) {
			libdir_suffix = "64";
		}
		ctx.status = PARSER_STATUS.READY;
		writeln (
			" - Parsing ChangeLog.txt for %s%s-%s".format(
				ctx.distro,
				libdir_suffix,
				ctx.dist_version
			)
		);
	}
	
	void parsePackageList() {
		path = ctx.datadir ~ "/slackware/raw/PACKAGES_Slackware%s-%s.txt".format(libdir_suffix, ctx.dist_version);
		
		if ( exists(path) == false) {
			writeln(" - '%s' file does not exist!".format(path));
			return;
		}
		auto f = rawData(path);
		auto lines = f.splitLines();
		
		for (auto i = 0; i < lines.length; i++) {
			if ( lines[i].startsWith("PACKAGE NAME:") == true) {
				auto s = lines[i].findSplit(": ")[2];
				auto pn = parsePackageName(s.strip());
				PackageData pkg = {
					name:pn(PKG_FILENAME_NAME),
					current_version:pn(PKG_FILENAME_VERSION),
					arch:pn(PKG_FILENAME_ARCH),
					release: ctx.dist_version
				};
				
				for (auto readahead = 1; readahead < 16; readahead++) {
					auto datapoint = lines[i+readahead].findSplit(": ");
					if (datapoint[0] == "") {
						break;
					}
					switch (datapoint[0]) {
						case "PACKAGE LOCATION": {break;}
						case "PACKAGE SIZE (compressed)": {break;} 
						case "PACKAGE SIZE (uncompressed)": {break;} 
						case "PACKAGE DESCRIPTION": {break;} 
						default: {pkg.description ~= datapoint[2] ~ "<br>"; break;}
					}
				}
				data.packages[pkg.name] = pkg;
			}
		}
	}
	
	bool matchUpdateLine(string s) {
		bool found = false;
		Regex!char[] patterns = [
			ctRegex!(`^.*\/.*-.*-.*-.*:.*$`),
			ctRegex!(`^.*\/linux-.*\/\*:.*$`)
		];
	
		foreach (pattern; patterns) {
			auto r = matchAll(s, pattern);
			if (!r.empty) {
				found = true;
			}
		}
		
		return found;
	}
	
	// this function skips past the notice to the first update
	// in the chunk
	int skipNotice(ref string[] lines) {
		int skip = 0;
		foreach (l; lines) {
			if ( matchUpdateLine(l)) {
				break;
			}
			skip++;
		}
		return skip;
	}
	
	PARSER_STATUS parseChangeLogChunk(in string dist_version, in DIST_ARCH dist_arch, string[] lines) {
		
		// call out to the date command
		// will need to find a better way to handle this
		int dt = convertDateStamp(lines[1]);
		
		for ( int i = skipNotice(lines); i < lines.length; i++ ) {
			auto l = lines[i];
			
			// if l is not an update then it must be notes
			// given after the previous update
			if ( matchUpdateLine(l) == false ) {
				data.changelog[$-1].notes ~= l ~ "<br>";
				if (l.count("(* Security fix *)") > 0) {
					data.changelog[$-1].security_fix = true;
				}
				continue;
			}
			
			ChangeLogEntry newchg;
			
			// Slackware only has a single official maintainer.
			// There are others, but their names are not reliably
			// exposed in the changelog
			newchg.author = "Patrick J. Volkerding <volkerdi@slackware.com>";
			auto spl = l.findSplit(": ");
			
			// make sure we exclude any leading
			// introduction for this chunk
			if ( spl[1] == "" ) {
				continue;
			}
			
			auto pkgname = parsePackageName(spl[0]);
			
			newchg.pkgname = pkgname(PKG_FILENAME_NAME);
			newchg.repository = pkgname(PKG_FILENAME_SERIES);
			newchg.new_version = pkgname(PKG_FILENAME_VERSION);
			newchg.port = pkgname(PKG_FILENAME_ARCH);
			newchg.change = spl[2];
			if (newchg.change.length > 20) {
				newchg.change = newchg.change[0 .. 20];
			}
			newchg.dist_version = dist_version;
			newchg.dist_arch = dist_arch;
			
			
			if (newchg.pkgname !in data.packages) {
				PackageData pkg;
				pkg.name = newchg.pkgname;
				pkg.arch = newchg.port;
				pkg.release = dist_version;
				data.packages[newchg.pkgname] = pkg;
			}
			newchg.date = dt;
			data.changelog ~= newchg;		
		}
		return PARSER_STATUS.INPROGRESS;
	}
	
	override void parseChangeLog() {
		parsePackageList();
		path = ctx.datadir ~ "/slackware/raw/ChangeLog_Slackware%s-%s.txt".format(libdir_suffix, ctx.dist_version);
		if ( exists(path) == false) {
			writeln(" - '%s' file does not exist!".format(path));
			return;
		}
		auto f = rawData(path);
		f = "\n" ~ f;
		
		auto chunks = f.split(SEP);
		
		foreach (c; chunks) {
			string[] lines = c.splitLines;
			// even a single line of data will result in
			// a file with 2 lines
			if (lines.length >= 2) {
				ctx.status = parseChangeLogChunk(ctx.dist_version, ctx.dist_arch, lines);
			}			

		}
		ctx.status = PARSER_STATUS.DONE;
		
		// sort the changelogs by date
		//sort!("a.date < b.date")(data.changelog);
	}
}
