import std.stdio  : writeln;
import std.file   : exists, write, append, remove;
import std.string : format;
import std.array  : join, replace;
import std.digest.sha;

import common;

/***********************************
 * interface for all DistroParser declarations
 *
 * The Parser interface provides a uniform way to access the fields of
 * a given DistroParser class. Applications that define multiple DistroParsers
 * can access them via Parser.
 *
 * See_Also:
 *    DistroParser
 */
interface Parser {
	
	void printChangelog();
	void saveChangeLog(OUTPUT_FORMAT fmt=CSV);
	void savePackageList(OUTPUT_FORMAT fmt=CSV);
	void saveForPostgres();
	ulong packageCount();
	ulong updateCount();
	
	void parseChangeLog();
}

/***********************************
 * Base class for parsing linux distro changelogs.
 *
 * This is the base class containing common functions to read/write
 * changelog data for Linux Distros. When adding support for a new distro
 * just subclass DistroParser and override functions as needed.
 *
 * See_Also:
 *    SlackwareParser, FedoraParser
 */
class DistroParser : Parser {
	PARSER_CONTEXT ctx;
	// struct used to store all ingested data	
	ParserData data;
	
	void printChangelog() {
		string s;
		foreach(chg; data.changelog) {
			s ~= "%d, %s, %s\n".format(chg.date, chg.pkgname, chg.author);
		}
		writeln(s);
	}

	ulong packageCount() {
		return data.packages.length; 
	}
	ulong updateCount() {
		return data.changelog.length; 
	}
	void saveChangeLog(OUTPUT_FORMAT fmt=CSV) {
		if (ctx.status != PARSER_STATUS.DONE) {return;}
		
		auto last = data.changelog[data.changelog.length-1];
		
		string csvfile = "%s/%s/%s-%s-%s-changelog.csv".format(
			ctx.datadir,
			ctx.distro,
			ctx.distro,
			ctx.dist_version,
			ctx.dist_arch
			
		);
		if (exists(csvfile)) {
			writeln(" - Changelog datafile already exists! BAILING OUT NOW");
			ctx.status = PARSER_STATUS.ERROR;
			return;
		}
		writeln(" - saving changelog to: ",csvfile);
		switch (fmt) {
			case CSV: {
				// don't print the header for right now
				//append(csvfile, "timestamp | maintainer | pkg_name | new_version | pkg_arch | dist_version | dist_arch | change_type | notes\n");
				foreach(chg; data.changelog) {
					append(csvfile, chg.getLine.join(" | ") ~ "\n");
				}
				break;
			}
			case JSON: {break;}
			default: {break;}
		}
	}
	void savePackageList(OUTPUT_FORMAT fmt=CSV) {
		if (ctx.status != PARSER_STATUS.DONE) {return;}
		
		auto last = data.changelog[data.changelog.length-1];
		
		string csvfile = "%s/%s/%s-%s-%s-packages.csv".format(
			ctx.datadir,
			ctx.distro,
			ctx.distro,
			ctx.dist_version,
			ctx.dist_arch
		);
		if (exists(csvfile)) {
			writeln(" - Package datafile already exists! BAILING OUT NOW");
			ctx.status = PARSER_STATUS.ERROR;
			return;
		}
		writeln(" - saving changelog to: ",csvfile);
		switch (fmt) {
		case CSV: {
			foreach(pkg; data.packages) {
				append(csvfile, "%s | %s | %s | %s\n".format(pkg.name, pkg.arch, pkg.release, pkg.description));
			}
			break;
		}
		case JSON: {break;}
		default: {break;}
		}
	}
	void parseChangeLog() {
		
	}
	
	void saveForPostgres() {
		if (ctx.status != PARSER_STATUS.DONE) {return;}
		auto last = data.changelog[data.changelog.length-1];
		string outputFile;
		
		
		outputFile = "%s/%s/%s-%s-%s-changelog.pgcopy".format(
			ctx.datadir,
			ctx.distro,
			ctx.distro,
			ctx.dist_version,
			ctx.dist_arch
		);
		if (exists(outputFile)) {
			writeln(" - updates pgcopy file already exists! removing..");
			remove(outputFile);
		}
		writeln(" - saving postgres COPY file to: ",outputFile);
		
		foreach(chg; data.changelog) {
			string line = "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n";
			int distid = 1;
			int distversionid = 1;
			line = format( line,
				timestampToString(chg.date).replace("Z", "+00"),
				chg.pkgname,
				chg.port,
				ctx.distro,
				ctx.dist_version,
				ctx.dist_arch,
				chg.new_version,
				chg.change,
				chg.author.replace("\\", "\\\\").replace("|", "\\|"),
				chg.notes.replace("\\", "\\\\").replace("|", "\\|").replace("\n", "<br>").replace("\r", "<br>"),
				chg.security_fix,
				chg.repository,
				timestampToString(chg.date)
			);
			append(outputFile, line);
		}
		
		outputFile = "%s/%s/%s-%s-%s-packages.pgcopy".format(
			ctx.datadir,
			ctx.distro,
			ctx.distro,
			ctx.dist_version,
			ctx.dist_arch
		);
		if (exists(outputFile)) {
			writeln(" - packages pgcopy file already exists! removing..");
			remove(outputFile);
		}
		
		writeln(" - saving package data to: ", outputFile);
		foreach(pkg; data.packages) {
			string line = format(
				"%s|%s|%s|%s|%s|%s\n",
				pkg.name,
				pkg.description.replace("|", "\\|"),
				ctx.distro,
				ctx.dist_version,
				ctx.dist_arch,
				pkg.arch,
			);
			append(outputFile, line);
		}
	}
}
