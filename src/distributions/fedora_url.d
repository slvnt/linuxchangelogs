import expat;
import std.stdio;
import std.string: toStringz, fromStringz, format, split, splitLines;
import std.conv : to;
import core.stdc.string : strlen;

import std.algorithm;
import std.file;

import std.typecons;
import std.net.curl;

alias FedoraMirrors = Tuple!(char[],char[]);


// Every Fedora release has 2 urls
// OS - Repo frozen at time of release
// UPDATES - all changes during post-release maintenance cycle
auto OS_URL_FSTRING = "https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-%s&arch=%s";
auto UPDATES_URL_FSTRING = "https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f%s&arch=%s";

auto ARCHIVE_URL_FSTRING = "https://archives.fedoraproject.org/pub/archive/fedora/linux/updates/%s/Everything/%s/";

// I'm bad at pointers... so this is a thing.
struct RepoData {
	string url;	
}


// uses the "API" to find a suitable mirror
// necessary because fedora moves their shit around in weird ways.
FedoraMirrors findMirror(string release, string arch) {
	auto content = get(OS_URL_FSTRING.format(release, arch)).splitLines;
	auto os = content[content.length - 1];
	content = get(UPDATES_URL_FSTRING.format(release, arch)).splitLines;
	auto updates = content[content.length - 1];
	
	foreach (url; content) {
		auto targetURL = url.findSplit("fedoraproject.org");
		if (targetURL[0] != url) {
			updates = url;
		}
	}
	//writeln(" - found upates url: ", updates);
	return tuple(os, updates);
}


/* element handlers used to parse Fedora's repodata files */
extern (C) void elementStart_Repodata(void *userData, const XML_Char *name, const XML_Char **atts) {
	int i;
	RepoData *r = cast(RepoData *)userData;
	cast(void)atts;
		
	// get the name of the element we are working on
	auto elementName = fromStringz(name);
	
	if ( *atts == null) {
		//warn("%s %s".format(elementName, "has no attributes!!"));
		//return;
	}
	switch (elementName) {
		case "repomd": {
			//writeln(elementName);
			break;
		}
		case "location": {
			for (i = 0; i < strlen(*atts).to!int; i += 2) {
				switch (fromStringz(atts[i])) {
					case "href":  { 
						string u = fromStringz(atts[i+1]).to!string;
						if (u.endsWith("other.xml.gz") != 0 || u.endsWith("OTHER.xml.gz") != 0) {
							r.url = u; 
						}
						break;
					}
					default: {break;}
				}
			}
			break;
		}
		default: {break;}
	}
}

extern (C) void elementEnd_Repodata(void *data, const XML_Char *name){
}




string parseMD (char[] url) {
	RepoData data;
	/* Begin parsing with Expat */
	XML_Parser parser;
	// hold status returned by expat
	XML_Status status;
	
	char[] xmlText;
	int len;

	parser = XML_ParserCreate(null);
	//XML_UseParserAsHandlerArg(parser);
	XML_SetUserData(parser, &data);
	XML_SetStartElementHandler(parser, &elementStart_Repodata);
	XML_SetEndElementHandler(parser, &elementEnd_Repodata);
	
	try  {
		xmlText = get(url);
	}
	catch (HTTPStatusException e) {
		// return empty string.
		// user will be notified later on.
		return data.url;
	}
	len = xmlText.length.to!int;
	
	status = XML_Parse(parser, xmlText.ptr, len, 1);
	
	if ( status == 0) {
		writeln("Cannot parse, file may be too large or not well-formed XML\n");
	}
	XML_ParserFree(parser);
	return data.url;
}



int main(string[] argv) {
	auto repomd = "repodata/repomd.xml";
	if (argv.length != 4) {
		return 1;
	}
	string release = argv[1];
	string arch = argv[2];
	int i = 0;
	
	
	auto urls = findMirror(release, arch);
	
	//RepoData repo;
	string u;
	if (argv[3] == "updates") {
		u = parseMD(urls[1]~repomd);
		if ( u == "") {return 3;}
		writeln(urls[1]~u);
	}
	else if (argv[3] == "os") {
		u = parseMD(urls[0]~repomd);
		if ( u == "") {return 3;}
		writeln(urls[0]~u);
	}
	else {
		return 2;
	}
	
	
	// sort the changelogs by date
	//sort!("a.date < b.date")(data.changelog);
	return 0;
}

