import expat;
import parser;
import common;
import common_dnf;

import std.stdio        : writeln;
import std.string       : toStringz, fromStringz, format, split;
import std.conv         : to;
import core.stdc.string : strlen;
import std.file         : exists;

import std.algorithm;


class RockyParser : DistroParser {
	XML_Parser parser;
	string path;
	string xmlText;
	
	// hold status returned by expat
	XML_Status status;
	
	this(PARSER_CONTEXT newctx) {
		ctx = newctx;
		writeln(" - Parsing repomd.xml for %s %s, %s".format(ctx.distro, ctx.dist_version, ctx.dist_arch));
		ctx.status = PARSER_STATUS.READY;
	}
	
	override void parseChangeLog() {			
		foreach(s; ["os"]) {
			writeln(" - Parsing data for ", s);
			parser = XML_ParserCreate(null);
			
			XML_SetUserData(parser, &data);
			XML_SetStartElementHandler(parser, &elementStart);
			XML_SetEndElementHandler(parser, &elementEnd);
			XML_SetCharacterDataHandler(parser, &textHandler);
			path = ctx.datadir ~ "/rocky/%s/pkgdata_rocky-%s-%s-%s.xml".format("raw", ctx.dist_version, ctx.dist_arch, s);
			if (exists(path)) {
				xmlText = rawData(path);
				int len = xmlText.length.to!int;
				
				status = XML_Parse(parser, xmlText.ptr, len, 1);
				ctx.status = PARSER_STATUS.DONE;
				if ( status == 0) {
					writeln("Cannot parse, file may be too large or not well-formed XML\n");
					ctx.status = PARSER_STATUS.ERROR;
				}
				
			}
			else {
				writeln("WARNING: Missing file for ", s);
				ctx.status = PARSER_STATUS.ERROR;
			}
			XML_ParserFree(parser);
		}
		
		// sort the changelogs by date
		sort!("a.date < b.date")(data.changelog);
	}
}


