import expat;
import common;
//import distro;

import std.stdio        : writeln;
import std.string       : toStringz, fromStringz, format, split;
import std.array        : join, replace;
import std.conv         : to;
import core.stdc.string : strlen;
import std.algorithm;

struct PackageAttributes {
	const(char)[] name;
	const(char)[] id;
}

// fedora puts the new version for each changelog entry into the 
// "author" field. This is a human readable string and text is not
// entered using a single spec

void parseAuthorString(string authorString, ref ChangeLogEntry chg) {
	auto spl = authorString.split(" ");
	auto last = spl.length - 1;
	if (count(spl[last], ">") > 0 || last == 0) {
		chg.author = authorString;
	} else {
		chg.author = spl[0 .. last - 1].join(" ");
		chg.new_version = spl[last];
	}
}


/*  Element handlers used to parse Fedora's "other" files */
extern (C) void elementStart(void *userData, const XML_Char *name, const XML_Char **atts) {
	int i;
	string s;
	ParserData *p = cast(ParserData *)userData;
	cast(void)atts;
	
	
	for (int indent = 0; indent < p.depth; indent++) {s ~= "  ";}
	
	// get the name of the element we are working on
	auto elementName = fromStringz(name);
	if ( *atts == null) {
		// maybe issue a warning here... but given the size of the repo
		// that can overwhelm the user with TMI.
		return;
	}
	switch (elementName) {
		case "package": {
			PackageData pkg;
			for (i = 0; i < strlen(*atts).to!int; i += 2) {
				switch (fromStringz(atts[i])) {
					case "name":  { pkg.name = fromStringz(atts[i+1]).to!string; break;}
					case "arch":  { pkg.arch = fromStringz(atts[i+1]).to!string; break;}
					case "pkgid": { pkg.id = fromStringz(atts[i+1]).to!string; break;}
					default: {break;}
				}
			}
			string new_pkg = "%s-%s".format(pkg.name,pkg.arch);
			
			// probably don't need to do this
			//if (new_pkg in p.packages) {
			//	writeln("ERROR: ", new_pkg);
			//}
			
			p.cur_pkg = new_pkg;		
			p.pkgCount += 1;				
			p.packages[new_pkg] = pkg;
			break;
		}
		case "version": {
			// pkg in progress, retrieve index
			int cur = p.packages.length.to!int - 1;
			for (i = 0; i < strlen(*atts).to!int; i += 2) {
				switch (fromStringz(atts[i])) {
					case "ver":  { p.packages[p.cur_pkg].current_version = fromStringz(atts[i+1]).to!string; break;}
					case "rel":  { p.packages[p.cur_pkg].release = fromStringz(atts[i+1]).to!string; break;}
					default: {break;}
				}
			}
			break;
		}
		case "changelog": {
			
			// pkg in progress, get a reference
			auto cur = p.packages[p.cur_pkg];
			ChangeLogEntry chg;
			chg.pkgname = cur.name;
			chg.port = cur.arch;
			chg.dist_version = cur.release;
			chg.dist_arch = p.dist_arch;
			chg.repository = p.repository;
			
			// parse attributes for <changelog>
			for (i = 0; i < strlen(*atts).to!int; i += 2) {
				switch (fromStringz(atts[i])) {
					case "author": {
						parseAuthorString(fromStringz(atts[i+1]).to!string, chg);
						break;
					}
					case "date"  : {chg.date = fromStringz(atts[i+1]).to!long; break;}
					default: {break;}
				}
			}
			
			p.changelog ~= chg;
			p.depth += 1;
			break;
		}
		default: {break;}
	}
}

extern (C) void elementEnd(void *data, const XML_Char *name){
	ParserData *p = cast(ParserData *)data;
	int l = p.packages.length.to!int;
	if (fromStringz(name) == "changelog") {
		p.depth -= 1;
	}
} 
extern (C) void textHandler(void *data, const XML_Char *s, int len){
		ParserData *p = cast(ParserData *)data;
		/*
			* Not sure why I keep getting data with a len of 6
			* There's something going on with my expat handlers that 
			* I don't fully understand.
			* 
			* The data is garbage and I need to check depth anyway, so 
			* just ignore it.
		*/
		if (p.depth == 1 && len != 6) {
			// get index for most recent changelog entry
			auto last = p.changelog.length - 1;
			p.changelog[last].notes ~= s[0 .. len].replace("|", "\"|\"");
		}
}
