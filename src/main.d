// imports from D
import std.stdio  : writeln, writefln;
import std.string : format;
import std.getopt;
import std.conv;
import std.process : environment;
import std.path;

// internal imports
import common;
import parser;
import distributions;

PARSER_CONTEXT parseArgs(string[] args) {
	PARSER_CONTEXT ctx;
	auto dataPath = environment.get("LCP_ROOT");
	if (dataPath !is null && isValidPath(dataPath)) {
		ctx.datadir = dataPath;
	}
	
	try {
		auto options = getopt (
			args,
			std.getopt.config.passThrough,
			"d", "Linux distro to parse", &ctx.distro,
			"r", "dist_version", &ctx.dist_version,
			"a", "dist_arch", &ctx.dist_arch
		);
		if (options.helpWanted) {
			defaultGetoptPrinter(
			"Usage: %s -d <slackware|fedora|debian> -r <dist_version> -a <dist_arch>\n".format(args[0]),
			options.options
			);
			ctx.status = PARSER_STATUS.ERROR;
		}
	} catch (ConvException e) {
		version(GDC_11_2_0) {
			writeln(e.msg);
		}
		else {
			writeln(e.message);
		}
		ctx.status = PARSER_STATUS.ERROR;
	}
	
	if (ctx.distro == DISTRO.none) {
		ctx.status = PARSER_STATUS.ERROR;
	}
	
	if (ctx.dist_arch == DIST_ARCH.none) {
		ctx.status = PARSER_STATUS.ERROR;
	}
	return ctx;
}

int main(string[] args) {
	PARSER_CONTEXT ctx = parseArgs(args);
	if ( ctx.status == PARSER_STATUS.ERROR) {
		return ctx.status;
	}
	// check to see if we are running on  my LGo server
	// while you can set this yourself it's unlikely you have will
	// have a matching directory structure
	auto lcp = environment.get("LCP", "no");
	
	Parser p;
	
	switch(ctx.distro) {
	case DISTRO.slackware: {
		p = new SlackwareParser(ctx);
		break;
	}
	case DISTRO.fedora: {
		p = new FedoraParser(ctx);
		break;
	}
	case DISTRO.rocky: {
		p = new RockyParser(ctx);
		break;
	}
	default: {break;}
	}
	
	p.parseChangeLog();
	if (lcp == "yes") {
		// we are running on an actual lcp server
		// output postgres files ONLY
		p.saveForPostgres();
	} else {
		p.saveChangeLog(CSV);
		p.savePackageList(CSV);	
	}
	
	if (ctx.status == PARSER_STATUS.ERROR) {
		writeln("program finished with error");
	} else {
		writefln(" - parsed %s updates for %s packages", p.updateCount, p.packageCount);
	}
	return ctx.status;
} 
