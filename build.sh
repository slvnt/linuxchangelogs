#!/bin/sh

if [ -d build ]; then
	rm -r build
fi

# use globally installed slacklib by default
LOCAL_SLACKLIB=false

# download any external dependencies
pkg-config --cflags libslacklib > /dev/null 2>&1
if [ $? == 1 ];  then
        cd subprojects/
        if [ ! -e slacklib-0.6.3.tar.bz2 ]; then
		wget https://gitlab.com/slvnt/slacklib/-/archive/0.6.3/slacklib-0.6.3.tar.bz2
        fi
        tar xvf slacklib-0.6.3.tar.bz2
	LOCAL_SLACKLIB=true
        cd ..
fi

# make sure each line of output gets a newline
TERM=dumb

mkdir build
cd build
  DFLAGS="-fPIC" \
  meson setup .. \
    --buildtype=release \
    --prefix=/usr \
    --sysconfdir=/etc \
    -Dstrip=true \
    -Dlocal_slacklib=$LOCAL_SLACKLIB
  "${NINJA:=ninja}"
cd ..


echo -e "\ndone building lcp."
if [ ! -e ./lcp ]; then
	ln -s build/lcp lcp
fi
echo -e "use ./download.sh to download changelog data\nand ./lcp to parse the data"
