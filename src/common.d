import std.file    : readText, exists, mkdirRecurse;
import std.string  : format, stripRight;
import std.conv    : to;
import std.stdio   : writeln;
import std.array   : replace, join, insertInPlace;
import std.process : execute;
import std.datetime;

// enum to hold global status of parser
enum PARSER_STATUS { 
	PREINIT = -1,
	READY = -2,
	INPROGRESS = -3,
	DONE = 0,
	ERROR = 1,
}

enum DISTRO {
	none,
	slackware,
	fedora,
	rocky,
};
enum DIST_ARCH {
	none,
	x86,
	x86_64,
	aarch64,
	i386,
};

struct PARSER_CONTEXT {
	DISTRO distro;
	DIST_ARCH dist_arch;
	string dist_version;
	string datadir = "LinuxChangelogs/";
	PARSER_STATUS status = PARSER_STATUS.PREINIT;
	string summary() {
		return "%s %s %s".format(distro, dist_arch, dist_version);
	}
}

struct PackageData {
	string name;
	string arch;
	string id;
	string current_version;
	string release;
	string description;
}

struct PackageVersion {
	int epoch;
	string ver;
	string rel;
}

struct ChangeLogEntry {
	string pkgname = "! bad entry !";
	string new_version;
	string dist_version;
	DIST_ARCH dist_arch;
	string port; 
	string author;
	long date;
	string change;
	string notes;
	string series;
	bool security_fix = false;
	string repository;
	string[] getLine() {
		string[] line = [
			date.to!string,
			author,
			pkgname,
			new_version,
			port,
			dist_version,
			to!string(dist_arch),
			change,
			security_fix.to!string,
			repository,
			notes.replace("\n","<br>")
		];
		return line;
	}
} 


struct ParserData {
	int depth;
	int pkgCount;
	string cur_pkg;
	DIST_ARCH dist_arch;
	string repository = "";
	PackageData[string] packages;
	ChangeLogEntry[] changelog;
}

// only CSV is implemented
alias OUTPUT_FORMAT = int;
enum {STDOUT = 0, CSV = 1, JSON=2, SLACKWARE=3}


string rawData(string path) {
	return readText(path);
}

string csvOutput (ParserData data) {
	// Not including header for now, 
	// programs later on the in  process are making assumptions.
	//string s = "name, version, release, arch\n";
	string s = "";
	
	string line = "%s,%s,%s,%s\n";
	foreach (pkg;  data.packages) {
		s ~= line.format(pkg.name, pkg.current_version, pkg.release, pkg.arch);
	}
	return s;
}

string timestampToString(long dt) {
	auto t = SysTime.fromUnixTime(dt, UTC());
	return t.toISOExtString;
}

/***********************************
 * string santize(string)
 *
 * input santization function. This function handles saving strings in a format
 * that postgres can handle via the COPY command
 *
 */
string sanitize(string s) {
	string cleanString = s;
	
	cleanString = cleanString.replace("\\", "\\\\");
	cleanString = cleanString.replace("|", "\\|");
	cleanString = cleanString.replace("\n", "<br>");
	cleanString = cleanString.replace("\r", "<br>");
	
	return cleanString;
}
