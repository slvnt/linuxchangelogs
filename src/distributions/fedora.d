import expat;
import parser;
import common;
import common_dnf;

import std.stdio        : writeln;
import std.string       : toStringz, fromStringz, format, split;
import std.conv         : to;
import core.stdc.string : strlen;
import std.file         : exists;

import std.algorithm;


// fedora puts the new package "version" in the "authors" attribute.
// we need to split the first/last name out as its own datapoint.
/*
 * There is no consistency to the format used in the authors attr
 * 
 * Foo Bar <foobar@example.tld> - major.minor.bugfix-build
 * Foo Bar <foobar@example.tld>  major.minor.bugfix-build
 * Foo Bar - major.minor.bugfix-build
 * Foo Bar major.minor.bugfix-build
 * foobar@example.tld - major.minor.bugfix-build
*/

class FedoraParser : DistroParser {
	XML_Parser parser;
	string path;
	string xmlText;
	
	// hold status returned by expat
	XML_Status status;
	
	this(PARSER_CONTEXT newctx) {
		ctx = newctx;
		writeln(" - Parsing repomd.xml for %s %s, %s".format(ctx.distro, ctx.dist_version, ctx.dist_arch));
		
		ctx.status = PARSER_STATUS.READY;
		data.dist_arch = ctx.dist_arch;
	}
	
	override void parseChangeLog() {
		// Every fedora release has 2 repositories:
		// a repo for the OS at time of release
		// a repo for all maintenance updates to the OS
					
		foreach(s; ["os", "updates"]) {
			writeln(" - Parsing data for ", s);
			data.repository = s;
			parser = XML_ParserCreate(null);
			
			XML_SetUserData(parser, &data);
			XML_SetStartElementHandler(parser, &elementStart);
			XML_SetEndElementHandler(parser, &elementEnd);
			XML_SetCharacterDataHandler(parser, &textHandler);
			path = ctx.datadir ~ "/fedora/%s/pkgdata_fedora-%s-%s-%s.xml".format("raw", ctx.dist_version, ctx.dist_arch, s);
			writeln(" - ", path);
			if (exists(path)) {
				xmlText = rawData(path);
				int len = xmlText.length.to!int;
				
				status = XML_Parse(parser, xmlText.ptr, len, 1);
				ctx.status = PARSER_STATUS.DONE;
				if ( status == 0) {
					writeln("Cannot parse, file may be too large or not well-formed XML\n");
					ctx.status = PARSER_STATUS.ERROR;
				}
				
				
			}
			else {
				writeln("WARNING: Missing file for ", s);
				ctx.status = PARSER_STATUS.ERROR;
			}
			XML_ParserFree(parser);
		}
		
		// sort the changelogs by date
		//sort!("a.date < b.date")(data.changelog);
	}
}


